open! Base
open! Merbocop_lib

let dbgf f =
  Fmt.kstr (Caml.Format.eprintf "@[<2>[merbocop-edit-ci-debug]@ %s@]@.%!") f

module Path = struct include Caml.Filename end

let ( // ) = Path.concat

module Job = struct
  type t =
    { name: string
    ; docker_image: string
    ; artifacts: string list
    ; dependencies: string list
    ; script: string list
    ; only_schedule: bool }

  let make ?(only_schedule = false) ?(dependencies = []) ~docker_image ~script
      ~artifacts name =
    {name; docker_image; artifacts; only_schedule; script; dependencies}
end

module Rendered = struct
  type file = {path: string; content: string list}

  let file path content = {path; content}

  type branch =
    { name: string
    ; files: file list
    ; cron: string option
    ; required_variables: string list }

  type t = {branches: branch list}

  let branch_names {branches} = List.map branches ~f:(fun b -> b.name)

  let pp_hum ppf {branches} =
    let open Fmt in
    pf ppf "@[<v># Pipeline@." ;
    List.iter branches ~f:(fun {name; files; cron; required_variables} ->
        pf ppf "## Branch `%s` (cron: %S, variables: %a)@." name
          (Option.value ~default:"N/A" cron)
          Fmt.Dump.(list string)
          required_variables ;
        List.iter files ~f:(fun {path; content} ->
            pf ppf "File `%s`:@." path ;
            pf ppf "@.```@.%a@.```@.@." (vbox (list ~sep:cut string)) content ) ) ;
    pf ppf "@]"
end

module Pipeline = struct
  type t =
    { branch: string
    ; description: string list
    ; jobs: Job.t list
    ; schedule: [`Every_n_hours of int | `On_hours of int list]
    ; required_variables: string list }

  let make ?(description = []) ?(schedule = `Every_n_hours 2)
      ?(required_variables = []) ~branch jobs =
    {branch; description; jobs; schedule; required_variables}

  let expected_cron pipeline =
    match pipeline.schedule with
    | `Every_n_hours 1 -> "0 * * * *"
    | `Every_n_hours n -> Fmt.str "0 */%d * * *" n
    | _ -> assert false
end

module Systemd_user = struct
  let make_service_file ~description ~command ~wants =
    [ "[Unit]"; Fmt.str "Description=%s" description; Fmt.str "Wants=%s" wants
    ; ""; "[Service]"; "Type=oneshot"; Fmt.str "ExecStart=%s" command
    ; (* Fmt.str "RestartSec = %d" restart_seconds ; "Restart = always"; *) ""
      (* We do not want an Install section here:
         https://unix.stackexchange.com/questions/680669/oncalendar-systemd-timer-unit-still-executes-at-boot-how-to-stop-it *)
      (* ; "[Install]"; "WantedBy=default.target" *) ]

  let make_timer_file ~description ~unit ~on_calendar ~requires =
    [ "[Unit]"; Fmt.str "Description=%s" description
    ; Fmt.str "Requires=%s" requires; ""; "[Timer]"; Fmt.str "Unit=%s" unit
    ; "Persistent=false"; Fmt.str "OnCalendar=%s" on_calendar
    ; (* Fmt.str "RestartSec = %d" restart_seconds ; "Restart = always"; *) ""
    ; "[Install]"; "WantedBy=timers.target" ]
end

module Systemd_generation = struct
  module File = struct
    type t = {path: string; content: string list; chmod: string}

    let make ?(chmod = "600") (path, content) = {path; content; chmod}
    let regular ?(chmod = "600") path content = {path; content; chmod}
    let script ?(chmod = "700") (path, content) = {path; content; chmod}
  end

  module Section = struct
    type t = {name: string; description: string list; files: File.t list}

    let make name ~description files = {name; description; files}
  end

  module Configuration = struct
    type t = {topscript: File.t; sections: Section.t list}

    let make topscript sections = {topscript; sections}

    let to_markdown ppf self =
      let outfile file =
        let open File in
        Fmt.pf ppf "### File `%s`\n\n```sh\n%s\n```\n\n" file.path
          (String.concat ~sep:"\n" (List.map ~f:(Fmt.str " %s") file.content))
      in
      let open Section in
      Fmt.pf ppf "## Toplevel Script\n\n" ;
      outfile self.topscript ;
      List.iter self.sections ~f:(fun section ->
          Fmt.pf ppf "## Pipeline `%s`\n\n" section.name ;
          List.iter section.description ~f:(Fmt.pf ppf "%s\n") ;
          Fmt.pf ppf "\n\n" ;
          List.iter section.files ~f:outfile ) ;
      ()

    let all_files self =
      self.topscript
      :: List.concat_map self.sections ~f:(fun sec -> sec.Section.files)
  end

  let of_pipelines ~path_prefix pipelines =
    let sysdconf = path_prefix // ".config/systemd/user" in
    let scripts = path_prefix // ".local/bin/" in
    let scripts_installed_systemd =
      (* https://serverfault.com/questions/794922/how-to-use-the-home-environment-variable-in-systemd-service-files
         Using `%h` for `--user` services seems OK. *)
      "%h/.local/bin/" in
    let scripts_installed_shell =
      (* https://serverfault.com/questions/794922/how-to-use-the-home-environment-variable-in-systemd-service-files
         Using `%h` for `--user` services seems OK. *)
      "$HOME/.local/bin/" in
    let merboconf = "$HOME/.config/merbocop/sh.env" in
    let monitoring_pipeline_name = "meta-monitoring-merbocop-pipelines" in
    let topscript_name = "mpm" in
    let topscript =
      let commands = ref [] in
      let command name body ~description =
        commands := (name, body, description) :: !commands in
      let command_case (name, body, _) =
        Fmt.str "  \"%s\" )" name :: body @ ["    ;;"] in
      let on_pipeline_names ~f =
        f monitoring_pipeline_name
        @ List.concat_map pipelines ~f:(fun pipe -> f pipe.Pipeline.branch)
      in
      command "wtf" ~description:"Quickly output status of the pipelines."
        ( on_pipeline_names ~f:(fun pipe ->
              [ Fmt.str "echo '## Pipeline %s:'" pipe
              ; Fmt.str
                  "echo \"  - service: $(systemctl --user is-active \
                   %s.service)\""
                  pipe
              ; Fmt.str
                  "echo \"  - timer: $(systemctl --user is-active %s.timer)\""
                  pipe
              ; Fmt.str "journalctl --user -n 5 -u %s | sed 's/^/  | /'" pipe ] )
        @ ["systemctl --user --all list-timers"]
        @ ["echo Done."] ) ;
      command "report"
        ~description:
          "Make a monitoring Markdown report (optional first argument: output \
           file)."
        (let show_command s =
           [ Fmt.str "printf '\\n```\\n $ %%s\\n' '%s'" s
           ; Fmt.str "%s 2>&1 | sed 's/^/| /'" s; Fmt.str "printf '\\n```\\n'"
           ] in
         ["output=${2:-/dev/stdout}"; "{"]
         @ [Fmt.str "printf '# Report %%s\\n\\n' \"$(date -R)\""]
         @ Fmt.kstr show_command "systemctl --user --all list-timers"
         @ Fmt.kstr show_command "df -h"
         @ Fmt.kstr show_command "docker ps"
         @ Fmt.kstr show_command "free -hl"
         @ on_pipeline_names ~f:(fun pipe ->
               [Fmt.str "printf '\\n## Pipeline %s\\n\\n'" pipe]
               @ Fmt.kstr show_command "systemctl --user status %s.service" pipe
               @ Fmt.kstr show_command "systemctl --user status %s.timer" pipe )
         @ ["} > \"$output\""] ) ;
      let start name subdesc filter =
        command (Fmt.str "start-%s" name)
          ~description:(Fmt.str "Enable and start %s." subdesc)
          ( on_pipeline_names ~f:(fun pipe ->
                if filter pipe then
                  [ Fmt.str "echo 'Starting timer %s'" pipe
                  ; Fmt.str "systemctl --user enable %s.timer" pipe
                    (* ; Fmt.str "systemctl --user start %s.timer" pipe.branch *)
                    (* ; Fmt.str "systemctl --user start %s --no-block" pipe.branch *)
                  ]
                else [] )
          @ ["echo Done."] ) in
      start "all" "all the pipelines" (fun _ -> true) ;
      start "test" "the testing pipelines" (fun p ->
          not (String.is_substring p ~substring:"-prod-") ) ;
      command "stop-all" ~description:"Stop and disable all the pipelines."
        ( on_pipeline_names ~f:(fun pipe ->
              [ Fmt.str "echo 'Stopping pipeline %s'" pipe
              ; Fmt.str "systemctl --user stop %s.service" pipe
              ; Fmt.str "systemctl --user disable %s.timer" pipe ] )
        @ ["echo Done."] ) ;
      command "warm-up" ~description:"Pull docker images, reload systemd-user."
        (let images =
           List.concat_map pipelines ~f:(fun pipe ->
               List.map pipe.jobs ~f:(fun job -> job.Job.docker_image) )
           |> List.dedup_and_sort ~compare:String.compare in
         List.concat_map images ~f:(fun image ->
             [ Fmt.str "echo '### Pulling  %s'" image
             ; Fmt.str "docker pull %s" image ] )
         @ ["echo '### Reloading systemd'"; "systemctl --user daemon-reload"]
         @ ["echo Done."] ) ;
      let show_help =
        ["echo Commands"]
        @ List.map !commands ~f:(fun (name, _, description) ->
              Fmt.str "printf \"* '%%s': %%s\\n\" %s %s" (Path.quote name)
                (Path.quote description) )
        @ ["echo Help:Done"] in
      command "help" ~description:"Show help" show_help ;
      let full_script =
        ["#!/bin/sh"; "case $1 in"]
        @ List.concat_map !commands ~f:command_case
        @ [ " * ) echo \"Command not understood: $@,\
            \ try 'help'\" >&2 ; exit 3 ;;"; "esac" ] in
      File.script (scripts // topscript_name, full_script) in
    let check_variable v =
      [ Fmt.str "if [ \"$%s\" = \"\" ] ; then" v
      ; Fmt.str "  echo 'Variable `$%s` is not defined!' >&2" v
      ; Fmt.str "  exit 2"; Fmt.str "fi" ] in
    let monitoring_pipeline =
      let name = monitoring_pipeline_name in
      let service_name = name ^ ".service" in
      let timer_name = name ^ ".timer" in
      let script_name = name in
      let script = scripts // script_name in
      let script_installed = scripts_installed_systemd // script_name in
      let on_calendar =
        Fmt.str "*-*-* %s:55:00"
          ( List.init 3 ~f:(fun i -> Fmt.str "%d" (i * 8))
          |> String.concat ~sep:"," ) in
      Section.make name
        ~description:["Extra pipeliner to monitor the real pipelines."]
        [ File.make
            ( sysdconf // service_name
            , Systemd_user.make_service_file ~wants:timer_name
                ~description:
                  (Fmt.str "%s" name)
                  (* ~restart_seconds:
                     ( match pipe.schedule with
                     | `Every_n_hours n -> n * 60 * 60 ) *)
                ~command:(Fmt.str "sh %s" script_installed) )
        ; File.regular (sysdconf // timer_name)
            (Systemd_user.make_timer_file
               ~description:(Fmt.str "Merbocop-timer-%s" name)
               ~requires:service_name ~unit:service_name ~on_calendar )
        ; File.script
            (let date_format = "+%Y%m%d-%H%M%S" in
             ( script
             , ["#! /bin/sh"; Fmt.str ". \"%s\"" merboconf]
               @ List.concat_map ["merbocout"] ~f:check_variable
               @ [ Fmt.str
                     "output=\"$merbocout/monitoring-report-$(date %S).md\""
                     date_format; Fmt.str "echo \"Making $output\""
                 ; Fmt.str "%s/%s report \"$output\"" scripts_installed_shell
                     topscript_name
                 ; Fmt.str "${after_report:-wc -l} \"$output\"" ] ) ) ] in
    Configuration.make topscript
      (monitoring_pipeline
       ::
       List.map pipelines ~f:(fun pipe ->
           let open Pipeline in
           let script_name = Fmt.str "merbocop-%s" pipe.branch in
           let script = scripts // script_name in
           let script_installed = scripts_installed_systemd // script_name in
           let service_name = Fmt.str "%s.service" pipe.branch in
           let timer_name = Fmt.str "%s.timer" pipe.branch in
           let on_calendar =
             let day = "*-*-*" in
             let make hours mins =
               Fmt.str "%s %s:%s:00" day
                 (String.concat ~sep:"," (List.map hours ~f:(Fmt.str "%d")))
                 (String.concat ~sep:"," (List.map mins ~f:(Fmt.str "%d")))
             in
             let hours =
               match pipe.schedule with
               | `Every_n_hours _ -> assert false
               | `On_hours hours -> hours in
             let reports =
               String.is_substring pipe.branch ~substring:"reports" in
             let prod = String.is_substring pipe.branch ~substring:"-prod-" in
             let test = String.is_substring pipe.branch ~substring:"-test-" in
             let fake = String.is_substring pipe.branch ~substring:"fake-" in
             let mins =
               match (reports, prod, test, fake) with
               | true, _, _, _ -> [0]
               | _, true, _, _ -> [20]
               | _, _, true, false -> [40]
               | _, _, _, true -> List.init 20 ~f:(fun i -> i * 3)
               | _ -> assert false in
             make hours mins in
           Section.make pipe.branch ~description:pipe.description
             [ File.make
                 ( sysdconf // service_name
                 , Systemd_user.make_service_file ~wants:timer_name
                     ~description:
                       (Fmt.str "Merbocop-schedule-%s" pipe.branch)
                       (* ~restart_seconds:
                          ( match pipe.schedule with
                          | `Every_n_hours n -> n * 60 * 60 ) *)
                     ~command:(Fmt.str "sh %s" script_installed) )
             ; File.regular (sysdconf // timer_name)
                 (Systemd_user.make_timer_file
                    ~description:(Fmt.str "Merbocop-timer-%s" pipe.branch)
                    ~requires:service_name ~unit:service_name ~on_calendar )
             ; File.script
                 ( script
                 , ["#! /bin/sh"; "set -e"; Fmt.str ". \"%s\"" merboconf]
                   @ List.concat_map
                       ("merbocout" :: pipe.required_variables)
                       ~f:check_variable
                   @ [Fmt.str "mkdir -p \"$merbocout/%s\"" pipe.branch]
                   @ List.concat_map pipe.jobs ~f:(fun job ->
                         let open Job in
                         [ Fmt.str "echo JOB-%s" job.name
                         ; Fmt.str
                             "docker run --rm %s $extra_docker_options -v \
                              \"$merbocout/%s:/data\" --workdir /data %s sh -c \
                              %s"
                             ( List.map pipe.required_variables ~f:(fun v ->
                                   Fmt.str "-e \"%s=$%s\"" v v )
                             |> String.concat ~sep:" " )
                             pipe.branch
                             (Path.quote job.docker_image)
                             (Path.quote (String.concat ~sep:" ; " job.script))
                         ] ) ) ] ) )
end

let render_pipelines pipelines =
  let open Rendered in
  let open Pipeline in
  let branches =
    List.map pipelines ~f:(fun pipeline ->
        let files =
          [ file "README.md"
              ( [ "# Merbocop CI Branch (DO NOT EDIT)"
                ; "This branch is managed by `src/edit-ci/main.ml`."; "" ]
              @ pipeline.description )
          ; file ".gitlab-ci.yml"
              ( ["# See README.md"; "stages:"; "  - run"]
              @ List.concat_map pipeline.jobs
                  ~f:
                    Job.(
                      let indent = List.map ~f:(Fmt.str "  %s") in
                      let section name l = name :: indent l in
                      let indented_dashes l =
                        indent (List.map l ~f:(Fmt.str "- %s")) in
                      fun job ->
                        Fmt.str "%s:" job.name
                        ::
                        indent
                          ( [ "stage: run"; Fmt.str "image: %s" job.docker_image
                            ; "artifacts:" ]
                          @ indent ("paths:" :: indented_dashes job.artifacts)
                          @ ( match job.dependencies with
                            | [] -> []
                            | deps ->
                                ["dependencies:"] @ indented_dashes deps
                                @ ["needs:"] @ indented_dashes deps )
                          @ ( if job.only_schedule then
                              section "only:"
                                (section "variables:"
                                   ["- $CI_PIPELINE_SOURCE == \"schedule\""] )
                            else [] )
                          @ section "script:"
                              (List.map job.script ~f:(Fmt.str "- %s")) )) ) ]
        in
        { name= pipeline.branch
        ; files
        ; cron= Some (expected_cron pipeline)
        ; required_variables= pipeline.required_variables } ) in
  {branches}

module Default_remote = struct
  let name () =
    match Caml.Sys.getenv "edit_ci_remote" with
    | s -> s
    | exception _ -> "test-remote"

  let ensure () = Fmt.kstr System.command_unit_or_fail "git fetch %s" (name ())
end

module Testable = struct
  let on = ref false

  let () =
    match Caml.Sys.getenv "testing_edit_ci" with
    | "true" -> on := true
    | _ | (exception _) -> on := false

  let remote_name () = Default_remote.name ()
  let git_remote_ref s = Fmt.str "refs/remotes/%s/%s" (remote_name ()) s
  let git_branch_with_remote s = Fmt.str "%s/%s" (remote_name ()) s

  let extra_pipelines () =
    if !on then
      let script =
        [ {|echo CI_PIPELINE_SOURCE $CI_PIPELINE_SOURCE|}; "mkdir artifact0"
        ; "echo world > artifact0/hello" ] in
      [ Pipeline.make ~branch:"fake-test-one"
          ~schedule:(`On_hours (List.init 24 ~f:Fn.id))
          ~required_variables:["a_variable"; "another_variable"]
          ~description:["This is a fake pipeline."]
          [ Job.make "job-cronly" ~docker_image:"alpine" ~only_schedule:true
              ~artifacts:["artifact0"] ~script
          ; Job.make "job1" ~docker_image:"ubuntu" ~artifacts:["artifact0"]
              ~script ] ]
    else []
end

let current_config : Pipeline.t list =
  let org = "oxheadalpha" in
  let image ~commit =
    Fmt.str "registry.gitlab.com/%s/merbocop:%s-run" org commit in
  let commit = "726995f7" in
  let test_image = image ~commit in
  let production_image = image ~commit in
  let production_projects = [("tezos-tezos", "3836952")] in
  let merbocop_project = ("oxheadalpha-merbocop", "14777851") in
  let test_projects = [("smondet-tezos", "9385204"); merbocop_project] in
  let all_projects = production_projects @ test_projects in
  let smbot_user = "4777401" in
  let tzbot_user = "4984456" in
  let cron image name script =
    Job.make ~only_schedule:true name ~docker_image:image ~artifacts:[name]
      ~script in
  let test_cron name = cron test_image name in
  let production_cron name = cron production_image name in
  let script ?(allow_failures = false) ~which name todo =
    let artifact_path = name in
    [ Fmt.str "echo %s"
        (Fmt.kstr Path.quote "Merbocop run %S (%s)" name
           (match which with `Test -> "Testing" | `Production -> "Production") )
    ; Fmt.str "mkdir -p %s" artifact_path
    ; Fmt.str
        "merbocop full-inspection --http-no-retry --dump-cache=%s/cache-dump/ \
         --write-body=%s/body.md %s %s"
        artifact_path artifact_path
        (if allow_failures then "--allow-failures" else "")
        (let whole project n = Fmt.str " --whole=%s:%d" project n in
         let user id token = Fmt.str " --self-id=%s --access=%s" id token in
         match todo with
         | `Report project -> whole project 80
         | `Comment (project, user_id, user_token, kind_of_run) ->
             whole project 80 ^ user user_id user_token ^ " --post-on-self"
             ^ Fmt.str " --post-warnings-to %s:%d:%s" (snd merbocop_project) 51
                 ( match kind_of_run with
                 | `Test -> "non-empty"
                 | `Production -> "non-empty" )
             (* ^ "  --comments-require-resolve-thread " *)
         | `Label (project, user_id, user_token) -> (
             whole project 80 ^ user user_id user_token
             (* " --set-doc-label=doc-only --set-approved-label=approved" *)
             ^ " --set-doc-label=doc-only"
             ^
             match which with
             | `Test -> " --assign-reviewers --wip-status any "
             | `Production -> " --assign-reviewers --wip-status ready-only " )
         | `Approve (project, user_id, user_token) ->
             whole project 80 ^ user user_id user_token ^ " --approve-doc-only"
        ) ] in
  let report ~which (project_name, project_id) =
    let name = Fmt.str "report-%s" project_name in
    test_cron name (script ~which name (`Report project_id)) in
  let cron_and_user which =
    let f cron n which_script = cron n (script ~which n which_script) in
    match which with
    | `Test -> (f test_cron, smbot_user, "$smbot_token")
    | `Production -> (f production_cron, tzbot_user, "$tezbocop_token") in
  let do_commenting which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "comment-%s" project_name)
      (`Comment (project_id, user_id, user_token, which)) in
  let do_labeling which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "label-%s" project_name)
      (`Label (project_id, user_id, user_token)) in
  let do_approving which (project_name, project_id) =
    let f, user_id, user_token = cron_and_user which in
    f
      (Fmt.str "approve-%s" project_name)
      (`Approve (project_id, user_id, user_token)) in
  let markdown_itemize_projects projects =
    List.map projects ~f:(fun (n, i) -> Fmt.str "* `%s` (id: `%s`)." n i) in
  let build_markdown_bodies jobs =
    let open Job in
    let output = "bodies" in
    let builds =
      List.concat_map jobs ~f:(fun job ->
          List.map job.artifacts ~f:(fun p ->
              Fmt.str "pandoc %s/body.md -s --metadata title=%s -o %s/%s.html" p
                p output p ) ) in
    jobs
    @ [ make "build-html-bodies" ~only_schedule:true ~artifacts:[output]
          ~docker_image:"ubuntu"
          ~dependencies:(List.map jobs ~f:(fun j -> j.name))
          ~script:
            ( [ "apt update"; "apt install --yes pandoc"
              ; Fmt.str "mkdir -p %s" output ]
            @ builds ) ] in
  let hours count start ofs =
    `On_hours (List.init count ~f:(fun i -> start + (i * ofs))) in
  [ Pipeline.make ~branch:"ci-cron-reports" ~schedule:(hours 4 0 6)
      ~description:
        ("This pipeline only runs “full” reports on various repositories:"
         :: "" :: markdown_itemize_projects all_projects )
      (List.map all_projects ~f:(report ~which:`Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-comments" ~schedule:(hours 4 1 6)
      ~description:
        ( [ "This cron, runs merbocop in commenting mode, with the testing"
          ; Fmt.str "docker-image (`%s`)" test_image; "on the testing projects:"
          ; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_commenting `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-labeling" ~schedule:(hours 4 2 6)
      ~description:
        ( [ "This cron, runs merbocop in `--set-*-label` mode, with the testing"
          ; Fmt.str "docker-image (`%s`)" test_image; "on the testing projects:"
          ; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_labeling `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-test-approving" ~schedule:(hours 4 3 6)
      ~description:
        ( [ "This cron, runs merbocop in `--approve-doc-only` mode,"
          ; "with the testing"; Fmt.str "docker-image (`%s`)" test_image
          ; "on the testing projects:"; "" ]
        @ markdown_itemize_projects test_projects )
      ~required_variables:["smbot_token"]
      (List.map test_projects ~f:(do_approving `Test) |> build_markdown_bodies)
  ; Pipeline.make ~branch:"ci-cron-prod-comments" ~schedule:(hours 6 0 4)
      ~description:
        ( [ "This cron, runs merbocop in commenting mode, with the production"
          ; Fmt.str "docker-image (`%s`)" production_image; "on:"; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_commenting `Production)
      |> build_markdown_bodies )
  ; Pipeline.make ~branch:"ci-cron-prod-labeling" ~schedule:(hours 12 1 2)
      ~description:
        ( [ "This cron, runs merbocop in `--set-*-label` mode, with the \
             production"; Fmt.str "docker-image (`%s`)" production_image; "on:"
          ; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_labeling `Production)
      |> build_markdown_bodies )
  ; Pipeline.make ~branch:"ci-cron-prod-approving" ~schedule:(hours 24 0 1)
      ~description:
        ( [ "This cron, runs merbocop in `--approve-doc-only` mode,"
          ; "with the production"
          ; Fmt.str "docker-image (`%s`)" production_image; "on:"; "" ]
        @ markdown_itemize_projects production_projects )
      ~required_variables:["tezbocop_token"]
      ( List.map production_projects ~f:(do_approving `Production)
      |> build_markdown_bodies ) ]
  @ Testable.extra_pipelines ()

let git_assert_remote_of_branch_exists branch =
  let _commit_hash =
    Fmt.kstr System.command_to_string_list_or_fail "git show-ref -s %s"
      (Testable.git_remote_ref branch) in
  ()

module Schedule = struct
  type t =
    {id: int; branch: string; cron: string; variables: (string * string) list}

  let all_current () =
    try
      let project_id =
        let remote = Default_remote.name () in
        let uri =
          Fmt.kstr System.command_to_string_list_or_fail "git remote get-url %s"
            remote
          |> String.concat ~sep:"" in
        uri
        |> String.chop_prefix_if_exists ~prefix:"git@gitlab.com:"
        |> String.chop_prefix_if_exists ~prefix:"https://gitlab.com/"
        |> String.chop_suffix_if_exists ~suffix:".git"
        |> Uri.pct_encode in
      let open Jq in
      let api_get path =
        Web_api.get_json
          ~private_token:
            ( try Caml.Sys.getenv "edit_ci_token"
              with Caml.Not_found ->
                Fmt.failwith "Missing edit_ci_token env-variable." )
          ~retry_options:Web_api.Retry_options.web_api ~pagination:false
          (Fmt.kstr Uri.of_string "https://gitlab.com/api/v4%s" path) in
      let json_all_schedules =
        Fmt.kstr api_get "/projects/%s/pipeline_schedules" project_id in
      let parse_schedule json =
        let dict = get_dict json in
        let id = get_field "id" dict |> get_int in
        let branch = get_field "ref" dict |> get_string in
        let cron = get_field "cron" dict |> get_string in
        let variables =
          let field =
            try get_field "variables" dict
            with _ ->
              Fmt.failwith
                "Cannot get the Schedule variables for %S, are you sure your \
                 token has enough access rights to see the project secrets?"
                branch in
          field
          |> get_list (fun obj ->
                 let dict = get_dict obj in
                 ( get_field "key" dict |> get_string
                 , get_field "value" dict |> get_string ) ) in
        {id; branch; cron; variables} in
      let with_details =
        let get_more_info_and_parse obj =
          let dict = get_dict obj in
          let id = get_field "id" dict |> get_int in
          Fmt.kstr api_get "/projects/%s/pipeline_schedules/%d" project_id id
          |> parse_schedule in
        get_list get_more_info_and_parse json_all_schedules in
      with_details
    with e ->
      Fmt.failwith "@[<2>Failed to get current schedules:@ %a@]" Exn.pp e
end

let get_current model =
  let open Rendered in
  let get_file ~branch relative_path =
    try
      let content =
        Fmt.kstr System.command_to_string_list_or_fail "git show %s"
          (Fmt.kstr Path.quote "%s:%s"
             (Testable.git_branch_with_remote branch)
             relative_path ) in
      Some {path= relative_path; content}
    with _ -> None in
  let branches =
    List.filter_map model.branches ~f:(fun branch ->
        try
          git_assert_remote_of_branch_exists branch.name ;
          let files =
            List.filter_map branch.files ~f:(fun f ->
                get_file ~branch:branch.name f.path ) in
          Some {name= branch.name; files; cron= None; required_variables= []}
        with _ -> None ) in
  {branches}

let show_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun () ->
        let rendered = render_pipelines current_config in
        Fmt.pr "%a%!" Rendered.pp_hum rendered ;
        dbgf "Done." )
    $ const () in
  (term, info "show" ~doc:"Show what it would ensure.")

let diff_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun () ->
        Default_remote.ensure () ;
        let model = render_pipelines current_config in
        let current = get_current model in
        let open Rendered in
        let all_extra_branches =
          let cmd = {|git branch -a --format="%(refname:short)"|} in
          System.command_to_string_list_or_fail cmd
          |> List.filter ~f:(fun line ->
                 String.is_prefix line
                   ~prefix:(Testable.git_branch_with_remote "ci-cron")
                 && not
                      (List.exists model.branches ~f:(fun br ->
                           String.equal
                             (Testable.git_branch_with_remote br.name)
                             line ) ) ) in
        let all_schedules = Schedule.all_current () in
        let ppf = Fmt.stdout in
        let outf fmt = Fmt.pf ppf fmt in
        outf "@[" ;
        List.iter model.branches ~f:(fun model_branch ->
            outf "@[<v>" ;
            match
              List.find current.branches ~f:(fun b ->
                  String.equal b.name model_branch.name )
            with
            | None ->
                outf "@,- Branch %s does not exist" model_branch.name ;
                outf "@]%!"
            | Some {files; _} ->
                outf "@,@[<v 4>- Branch %s is there" model_branch.name ;
                List.iter model_branch.files ~f:(fun {path; content} ->
                    outf "@,- File %S" path ;
                    match
                      List.find files ~f:(fun f -> String.equal f.path path)
                    with
                    | Some {content= oc; _}
                      when List.equal String.equal oc content ->
                        outf " is ready."
                    | None -> outf " is absent."
                    | Some _ -> outf " is different." ) ;
                let expected_cron =
                  model_branch.cron |> Option.value ~default:"MISSING" in
                begin
                  match
                    List.find all_schedules ~f:(function
                      | Schedule.{branch; _}
                        when String.equal branch model_branch.name ->
                          true
                      | _ -> false )
                  with
                  | Some Schedule.{cron; variables; _} ->
                      outf "@,- @[Existing schedule uses %a.@]" Fmt.text
                        ( if String.equal cron expected_cron then
                          Fmt.str "the right cron: %S" cron
                        else
                          Fmt.str "the WRONG cron: %S (expected: %S)." cron
                            expected_cron ) ;
                      outf "@,- @[Existing schedule defines %a@]." Fmt.text
                        ( if
                          List.for_all model_branch.required_variables
                            ~f:(fun v ->
                              List.exists variables ~f:(fun (n, _) ->
                                  String.equal v n ) )
                        then
                          Fmt.str "all the required variables (%a ⊂ %a)"
                            Fmt.Dump.(list string)
                            model_branch.required_variables
                            Fmt.Dump.(list string)
                            (List.map ~f:fst variables)
                        else
                          Fmt.str "NOT ENOUGH VARIABLES: %a ⊄ %a"
                            Fmt.Dump.(list string)
                            model_branch.required_variables
                            Fmt.Dump.(list string)
                            (List.map ~f:fst variables) )
                  | None ->
                      outf
                        "@,\
                         - @[There is no schedule, need to add one with@ cron \
                         %S and@ variables: %a@]"
                        expected_cron
                        Fmt.Dump.(list string)
                        model_branch.required_variables
                end ;
                outf "@]%!" ) ;
        List.iter all_extra_branches ~f:(fun name ->
            outf "@,@[<2>- %a@]" Fmt.text
              (Fmt.str
                 "Branch %S has a relevant prefix but is not currently handled."
                 name ) ) ;
        List.iter all_schedules ~f:(fun Schedule.{branch; cron; variables; _} ->
            if
              List.exists model.branches ~f:(fun b ->
                  String.equal b.name branch )
            then ()
            else
              outf
                "@,\
                 @[<2>- There is an orphan Schedule on@ branch %S@ (cron: %S,@ \
                 %s).@]"
                branch cron
                ( match variables with
                | [] -> "no variables"
                | more ->
                    Fmt.str "variables: %a"
                      Fmt.Dump.(list string)
                      (List.map more ~f:fst) ) ) ;
        outf "@,@]%!" ;
        dbgf "Done." )
    $ const () in
  (term, info "diff" ~doc:"Compare the existing with what would be created.")

let ensure_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun push_flag () ->
        Default_remote.ensure () ;
        let rendered = render_pipelines current_config in
        let worktree_path branch = "_worktree" // branch in
        let _log = ref [] in
        let logf depth fmt =
          Fmt.kstr (fun s -> _log := (depth, s) :: !_log) fmt in
        let open Rendered in
        Exn.protect
          ~f:(fun () ->
            List.iter rendered.branches ~f:(fun branch ->
                let local = Fmt.str "local-%s" branch.name in
                logf 0 "Branch %s" branch.name ;
                logf 1 "local: %s" local ;
                begin
                  try
                    git_assert_remote_of_branch_exists branch.name ;
                    Fmt.kstr System.command_unit_or_fail
                      "git branch -f %s -t %s" (Path.quote local)
                      (Path.quote (Testable.git_branch_with_remote branch.name)) ;
                    logf 2 "-> tracking %s"
                      (Testable.git_branch_with_remote branch.name)
                  with _ ->
                    Fmt.kstr System.command_unit_or_fail "git branch -f %s"
                      (Path.quote local) ;
                    logf 2 "-> freshly created, set upstream to %s"
                      (Testable.git_branch_with_remote branch.name)
                end ;
                let worktree = worktree_path local in
                Fmt.kstr System.command_unit_or_fail
                  "git worktree remove -f %s || :" (Path.quote worktree) ;
                Fmt.kstr System.command_unit_or_fail "git worktree add %s %s"
                  (Path.quote worktree) local ;
                logf 1 "Worktree created: %s" worktree ;
                List.iter branch.files ~f:(fun {path; content} ->
                    let full_path = worktree // path in
                    System.write_lines full_path content ;
                    Fmt.kstr System.command_unit_or_fail "git -C %s add %s"
                      worktree path ;
                    logf 1 "Wrote and added: %s" full_path ) ;
                let status =
                  Fmt.kstr System.command_to_string_list_or_fail
                    "git -C %s status --porcelain" (Path.quote worktree) in
                begin
                  match status with
                  | [] -> logf 1 "Nothing to commit."
                  | _ ->
                      logf 1 "Need to commit (%d files)" (List.length status) ;
                      let msg = Fmt.str "Update CI for %s" branch.name in
                      Fmt.kstr System.command_unit_or_fail
                        "git -C %s commit -m %s" (Path.quote worktree)
                        (Path.quote msg) ;
                      logf 1 "Commit: %S" msg ;
                      if push_flag then (
                        Fmt.kstr System.command_unit_or_fail
                          "git -C %s push %s %s:%s" (Path.quote worktree)
                          (Path.quote (Default_remote.name ()))
                          local (Path.quote branch.name) ;
                        logf 1 "PUSHED to %s / %s" (Default_remote.name ())
                          branch.name )
                      else logf 1 "Not pushing (use --push)."
                end ;
                () ) )
          ~finally:(fun () ->
            List.rev !_log
            |> List.iter ~f:(fun (depth, msg) ->
                   Fmt.pr "%s%s\n%!" (String.make (depth * 4) ' ') msg ) ) ;
        dbgf "Done." )
    $ Arg.(value (flag (info ["push"] ~doc:"All push the commits.")))
    $ const () in
  (term, info "ensure" ~doc:"Ensure that the setup is applied.")

let systemd_output_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun output_path output_format () ->
        match output_format with
        | `Markdown ->
            let ppf, close =
              match output_path with
              | None -> (Fmt.stdout, Fn.id)
              | Some p ->
                  let o = Caml.open_out p in
                  ( Caml.Format.formatter_of_out_channel o
                  , fun () -> Caml.close_out o ) in
            let config =
              Systemd_generation.of_pipelines ~path_prefix:"$HOME"
                current_config in
            Systemd_generation.Configuration.to_markdown ppf config ;
            close () ;
            ()
        | `Tar ->
            let tmp = Path.temp_file "merbocop-systemd" "" in
            let outtar = Fmt.str "%s.tar" tmp in
            let path_prefix = Fmt.str "%s.d" tmp in
            let config =
              Systemd_generation.of_pipelines ~path_prefix current_config in
            let files = Systemd_generation.Configuration.all_files config in
            List.iter files
              ~f:
                Systemd_generation.File.(
                  fun {path; content; chmod} ->
                    Fmt.epr "Writing %d lines in %s (chmod: %s)\n%!"
                      (List.length content) path chmod ;
                    Fmt.kstr System.command_unit_or_fail "mkdir -p %s"
                      Path.(quote (dirname path)) ;
                    System.write_lines path content ;
                    Fmt.kstr System.command_unit_or_fail "chmod %s %s" chmod
                      (Path.quote path)) ;
            Fmt.kstr System.command_unit_or_fail "cd %s ; tar cvf %s ."
              Path.(quote path_prefix)
              Path.(quote outtar) ;
            let made =
              match output_path with
              | Some s ->
                  Fmt.kstr System.command_unit_or_fail "mv %s %s"
                    (Path.quote outtar) s ;
                  s
              | None -> outtar in
            Fmt.epr "Made %s\n%!" made ; () )
    $ Arg.(
        value
          (opt (some string) None (info ["o"; "output"] ~doc:"Output path.")))
    $ Arg.(
        value
          (opt
             (enum [("markdown", `Markdown); ("tar", `Tar)])
             `Markdown
             (info ["f"; "format"] ~doc:"Output format.") ))
    $ const () in
  ( term
  , info "systemd-configuration"
      ~doc:"Generate the equivalent systemd configuration" )

let main () =
  let open Cmdliner in
  let version = "0.0.0" in
  let help =
    Term.
      ( ret (const (`Help (`Auto, None)))
      , info "merbocop-edit-ci" ~version ~doc:"Edit the .gitlab-ci.yml file." )
  in
  Term.exit
    ( try
        Term.eval_choice ~catch:false
          (help : unit Term.t * _)
          [ show_command (); diff_command (); ensure_command ()
          ; systemd_output_command () ]
      with Failure s ->
        Fmt.epr "@[<4>FAILURE:@ %a@]%!" Fmt.text s ;
        Caml.exit 3 )

let () = main ()
