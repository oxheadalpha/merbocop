open! Base

(** [post_or_edit_mr_comment] will post a Gitlab note (comment) [body_of_prev]
    on merge request [mr_result]. [do_edit_existing] will attempt to edit
    previous comments by matching [user_id] preexisting note before posting a
    new one. *)
let post_or_edit_mr_comment ?(do_edit_existing = true) ?(token : string option)
    ~(mr_result : Merge_request.mr_result) ~(user_id : int)
    ~(retry_options : Web_api.Retry_options.t) ~(warnings : Warnings.t)
    ~(reporting_only : bool) body =
  let open Merge_request in
  let my_comment =
    if not do_edit_existing then None
    else
      (* If do edit existing, look for merboco's previous comment. *)
      match mr_result with
      | Ok mr -> (
        match mr.comments with
        | Some comments -> Comment.of_user ~comments ~user_id
        | None -> None )
      | Error _ -> None in
  let note_id result =
    let open Jq in
    try get_dict result |> get_field "id" |> get_int
    with e ->
      Debug.dbg "Bot_actions.post_or_edit_mr_comment \n\n\n%s, %s\n\n"
        (Exn.to_string_mach e)
        (Ezjsonm.value_to_string result) ;
      raise e in
  let report_json id = Jq.(dict [("id", int id); ("reporting", `String "OK")]) in
  match (my_comment, mr_result) with
  | None, Ok {id; project; reports; _} ->
      (* If no previous comment, post new one. *)
      let add_result =
        if reporting_only then report_json 000000
        else
          Comment.add_merge_request_note ?token ~project:project.id ~mr:id
            ~retry_options body in
      let new_id = note_id add_result in
      Reporting.add_conditional reports
        (Fmt.str
           "No previous comment was found. A new comment was posted. mr:%d \
            note_id:%d"
           id new_id ) ;
      Ok (`Added add_result)
  | Some comment, Ok mr -> (
    (* Check if comment body is a new comment *)
    match Comment.is_new ~body ~comment with
    | false ->
        Reporting.add_conditional mr.reports
          (Fmt.str
             "A previous comment was found but wasn't edited because there was \
              nothing new to say mr:%d note_id:%d"
             mr.id comment.id ) ;
        Ok (`No_change comment.json)
    | true -> (
        let report_result = report_json comment.id in
        let edit_result =
          if reporting_only then report_result
          else
            Comment.edit_merge_request_note ?token ~project:mr.project.id
              ~mr:mr.id ~retry_options ~note:comment.id body in
        match edit_result with
        | `Float 500. ->
            let add_result =
              if reporting_only then report_result
              else
                Comment.add_merge_request_note ?token ~project:mr.project.id
                  ~mr:mr.id ~retry_options body in
            let new_id = note_id add_result in
            Reporting.add_conditional mr.reports
              (Fmt.str "Failed to edit comment with response code 500.") ;
            Warnings.add ~warnings ~mr
              ~s:
                (Fmt.str
                   "Failed to edit comment with response code 500. A new \
                    comment was posted mr:%d note_id:%d"
                   mr.id new_id )
              () ;
            Ok (`Added add_result)
        | _ ->
            let new_id = note_id edit_result in
            if Int.equal comment.id new_id then
              Reporting.add_conditional mr.reports
                (Fmt.str "A previous comment was edited. mr:%d note-id:%d" mr.id
                   comment.id )
            else
              begin
                Reporting.add_conditional mr.reports
                  (Fmt.str
                     "Attempted to edit a comment and might have posted a new \
                      one. There is a new note id. mr:%d note_id:%d new_id:%d"
                     mr.id comment.id new_id ) ;
                Warnings.add ~warnings ~mr
                  ~s:
                    (Fmt.str
                       "Attempted to edit a comment and might have posted a \
                        new one. There is a new note id. mr:%d note_id:%d \
                        new_id:%d"
                       mr.id comment.id new_id )
              end
                () ;
            Ok (`Edited edit_result) ) )
  | _, Error e -> Error e

(** [post_or_edit_mr_thread] will post a Gitlab discussion (thread) [body] on
    merge request [mr_result]. If [do_edit_existing] is true, Merbocop will
    attempt to edit preexisting thread note before posting a new one. *)
let post_or_edit_mr_thread ?(do_edit_existing = true) ?(token : string option)
    ~(mr_result : Merge_request.mr_result) ~(user_id : int)
    ~(retry_options : Web_api.Retry_options.t) ~(warnings : Warnings.t)
    ~(reporting_only : bool) body =
  let open Merge_request in
  let my_thread : (Thread.t * Comment.t) option =
    if not do_edit_existing then None
    else
      match mr_result with
      | Ok mr -> (
        match mr.threads with
        | Some threads -> Thread.of_user ~threads ~user_id
        | None -> None )
      | Error _ -> None in
  let thread_id result =
    let open Jq in
    try get_dict result |> get_field "id" |> get_string
    with e ->
      Debug.dbg
        "Merge_request.post_or_edit_mr_thread thread_id error \n\n\n%s, %s\n\n"
        (Exn.to_string_mach e)
        (Ezjsonm.value_to_string result) ;
      raise e in
  let report_thread id =
    Jq.(dict [("id", string id); ("reporting", `String "OK")]) in
  match (my_thread, mr_result) with
  | None, Ok mr ->
      let add_result =
        if reporting_only then report_thread "dry-run-no-thread-id"
        else
          Thread.start_merge_request_discussion ?token ~project:mr.project.id
            ~mr:mr.id ~retry_options body in
      let new_id = thread_id add_result in
      Reporting.add_conditional mr.reports
        (Fmt.str
           "No previous thread was found. A new thread was posted. mr:%d \
            thread-id:%s"
           mr.id new_id ) ;
      Ok (`Added add_result)
  | Some (thread, comment), Ok mr -> (
    match Comment.is_new ~body ~comment with
    | false ->
        Reporting.add_conditional mr.reports
          (Fmt.str
             "A previous thread was found but wasn't edited because there was \
              nothing new to say mr:%d thread_id:%s note_id:%d"
             mr.id thread.id comment.id ) ;
        Ok (`No_change comment.json)
    | true -> (
        let report_result = report_thread "dry-run-no-thread-id" in
        let edit_result =
          if reporting_only then report_result
          else
            Thread.edit_merge_request_discussion ?token ~project:mr.project.id
              ~mr:mr.id ~retry_options ~discussion:thread.id ~note:comment.id
              body in
        match edit_result with
        | `Float 500. ->
            let add_result =
              if reporting_only then report_result
              else
                Thread.start_merge_request_discussion ?token
                  ~project:mr.project.id ~mr:mr.id ~retry_options body in
            let new_id = thread_id add_result in
            Reporting.add_conditional mr.reports
              (Fmt.str
                 "Failed to edit a previous threat with response code 500. A \
                  new thread was posted mr:%d thread-id:%s"
                 mr.id new_id ) ;
            Warnings.add ~warnings ~mr
              ~s:
                (Fmt.str
                   "Failed to edit a previous threat with response code 500. A \
                    new thread was posted mr:%d thread-id:%s"
                   mr.id new_id )
              () ;
            Ok (`Added add_result)
        | _ ->
            let note_id result =
              let open Jq in
              try get_dict result |> get_field "id" |> get_int
              with e ->
                Debug.dbg
                  "Bot_actions.post_or_edit_mr_thread note_id error \n\n\n\
                   %s, %s\n\n"
                  (Exn.to_string_mach e)
                  (Ezjsonm.value_to_string result) ;
                raise e in
            let report_note id =
              Jq.(dict [("id", int id); ("reporting", `String "OK")]) in
            let new_id =
              note_id
                (if reporting_only then report_note comment.id else edit_result)
              (* API for editing a descussion note returns only the note JSON. So there is not thread id*)
            in
            if Int.equal comment.id new_id then
              Reporting.add_conditional mr.reports
                (Fmt.str
                   "A previous thread note was found and was edited. mr:%d \
                    thread-id:%s note-id:%d"
                   mr.id thread.id comment.id )
            else
              begin
                Reporting.add_conditional mr.reports
                  (Fmt.str
                     "Attempted to edit a thread and might have posted a new \
                      one. There is a new thread id. mr:%d previous \
                      thread-id:%s new note-id:%d"
                     mr.id thread.id new_id ) ;
                Warnings.add ~warnings ~mr
                  ~s:
                    (Fmt.str
                       "Attempted to edit a thread and might have posted a new \
                        one. There is a new thread id. mr:%d previous \
                        thread-id:%s new note-id:%d"
                       mr.id thread.id new_id )
              end
                () ;
            Ok (`Edited edit_result) ) )
  | _, Error e -> Error e

(** [post_or_edit_mr_comment_or_thread] will post a merge request thread if
    [require_resolve_thread] is true and will post a comment otherwize. *)
let post_or_edit_mr_comment_or_thread ?(require_resolve_thread = false)
    ?(do_edit_existing = true) ?(token : string option)
    ~(mr_result : Merge_request.mr_result) ~(user_id : int)
    ~(retry_options : Web_api.Retry_options.t) ~(warnings : Warnings.t)
    ~(reporting_only : bool) body_of_prev =
  if require_resolve_thread then
    post_or_edit_mr_thread ~do_edit_existing ?token ~mr_result ~user_id
      ~retry_options ~warnings ~reporting_only body_of_prev
  else
    post_or_edit_mr_comment ~do_edit_existing ?token ~mr_result ~user_id
      ~retry_options ~warnings ~reporting_only body_of_prev

(** [check_json message api_call] is the [api_call] : Ezjosnm.value or an
    exception. check_json m a will attempt to parse the JOSN data [a] returned
    by a call to the Gitlab API by checking the "id" field. A failure raises and
    exception with the message [m]. *)
let check_json ?(message = "Gitlab API call") (api_call : Ezjsonm.value) : Jq.t
    =
  let open Jq in
  try
    get_dict api_call |> get_field "id" |> get_int
    |> function (_ : int) -> api_call
  with e ->
    Debug.dbg "Failure: [%s] Error parsion JSON: \n\n\n%s\n\n" message
      (Exn.to_string_mach e) ;
    raise e

(** [label_mr token project mr labels] adds Gitlab [labels] to a merge request. *)
let label_mr ?(token : string option) ~(project : string)
    ~(mr : Merge_request.t) ~(retry_options : Web_api.Retry_options.t)
    ~(reporting_only : bool) (labels : string list) =
  let report_json =
    Ezjsonm.(dict [("id", int mr.id); ("reporting", `String "OK")]) in
  match
    List.filter labels ~f:(fun label ->
        List.mem mr.labels label ~equal:String.equal |> not )
  with
  | [] -> Ok None
  | labels -> (
    try
      Ok
        (Some
           ( ( if reporting_only then report_json
             else
               Gitlab.add_mr_labels ?token ~project ~mr_id:mr.id ~retry_options
                 labels )
           |> check_json
                ~message:
                  (Printf.sprintf "Gitlab.add_mr_labels project:%s mr:%d"
                     project mr.id ) ) )
    with e -> Error e )

(** [unlabel_mr token project mr labels] removes Gitlab [labels] from a merge
    request. *)
let unlabel_mr ?(token : string option) ~(project : string)
    ~(mr : Merge_request.t) ~(retry_options : Web_api.Retry_options.t)
    ~(reporting_only : bool) (labels : string list) =
  let report_json =
    Ezjsonm.(dict [("id", int mr.id); ("reporting", `String "OK")]) in
  match
    List.filter labels ~f:(fun label ->
        List.mem mr.labels label ~equal:String.equal )
  with
  | [] -> Ok None
  | labels -> (
    try
      Ok
        (Some
           ( ( if reporting_only then report_json
             else
               Gitlab.remove_mr_labels ?token ~project ~mr_id:mr.id
                 ~retry_options labels )
           |> check_json
                ~message:
                  (Printf.sprintf "Gitlab.remove_mr_labels project:%s mr:%d"
                     project mr.id ) ) )
    with e -> Error e )

(** [approve_mr user_id token project mr] Approves a Gitlab merge request. *)
let approve_mr ?(user_id : int option) ?(token : string option)
    ~(reporting_only : bool) ~(project : string)
    ~(retry_options : Web_api.Retry_options.t) (mr : Merge_request.t) =
  let report_json =
    Ezjsonm.(dict [("id", int mr.id); ("reporting", `String "OK")]) in
  let json =
    match user_id with
    | None ->
        Fmt.kstr
          (Merge_request.Reporting.add_conditional mr.reports)
          "Merbocop would have approved this MR. Note: `_User_ID_` was \
           missing. --approve-doc-only requires the --self-id option. \
           (Project:%s MR:%d)"
          project mr.id ;
        report_json
    | Some id -> (
      match List.mem mr.approvals.approved_by id ~equal:Int.equal with
      | true ->
          Fmt.kstr
            (Merge_request.Reporting.add_conditional mr.reports)
            "Merbocop has approved this MR already and wouldn't change \
             anything. (Project:%s MR:%d)"
            project mr.id ;
          report_json
      | false ->
          Fmt.kstr
            (Merge_request.Reporting.add_conditional mr.reports)
            "Merbocop would have approved this MR. (Project:%s MR:%d)" project
            mr.id ;
          if reporting_only then report_json
          else Gitlab.approve_merge_request ?token ~project ~retry_options mr.id
      ) in
  try
    Ok
      (Some
         ( json
         |> check_json
              ~message:
                (Printf.sprintf "Gitlab.approve_merge_request project:%s mr:%d"
                   project mr.id ) ) )
  with e -> Error e

(** [unapprove_mr user_id token project mr] Removes approval from a Gitlab merge
    request. *)
let unapprove_mr ?(user_id : int option) ?(token : string option)
    ~(reporting_only : bool) ~(project : string)
    ~(retry_options : Web_api.Retry_options.t) (mr : Merge_request.t) =
  let report_json =
    Ezjsonm.(dict [("id", int mr.id); ("reporting", `String "OK")]) in
  let json =
    match user_id with
    | None ->
        Fmt.kstr
          (Merge_request.Reporting.add_conditional mr.reports)
          "Merbocop wouldn't approved this MR. Note: `_User_ID_` was missing. \
           --approve-doc-only requires the --self-id option. (Project:%s \
           MR:%d)"
          project mr.id ;
        report_json
    | Some id -> (
      match List.mem mr.approvals.approved_by id ~equal:Int.equal with
      | false ->
          Fmt.kstr
            (Merge_request.Reporting.add_conditional mr.reports)
            "Merbocop wouldn't have approved this MR. (Project:%s MR:%d)"
            project mr.id ;
          report_json
      | true ->
          Fmt.kstr
            (Merge_request.Reporting.add_conditional mr.reports)
            "Merbocop would have revoked its approval form this merge request. \
             (Project:%s MR:%d)"
            project mr.id ;
          if reporting_only then report_json
          else
            Gitlab.unapprove_merge_request ?token ~project ~retry_options mr.id
      ) in
  try
    Ok
      (Some
         ( json
         |> check_json
              ~message:
                (Printf.sprintf
                   "Gitlab.unapprove_merge_request project:%s mr:%d" project
                   mr.id ) ) )
  with e -> Error e
