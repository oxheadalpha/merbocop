open! Base
open Fmt

let empty = ""
let h n s = [str "%s %s" (String.make n '#') s; empty]
let par l = l @ [empty]
let verbatim l = "```" :: l @ ["```"]
let plural = function 1 -> "" | _ -> "s"
let plural_list = function [_] -> "" | _ -> "s"
let plural_be = function 1 -> "is" | _ -> "are"
let third_person = function 1 -> "s" | _ -> ""
let hl = [""; String.make 10 '-'; ""]
let colon_or_dot = function 0 -> "." | _ -> ":"

let merge_team_doc_link =
  "http://tezos.gitlab.io/developer/contributing.html#the-merge-request-bot"

let todo_comment_doc_link =
  "https://tezos.gitlab.io/developer/guidelines.html#todo-fixme-comments"

let code_escape s =
  let max_consecutive_backquotes =
    String.fold s ~init:(0, 0) ~f:(fun (prev_c, prev_m) -> function
      | '`' -> (prev_c + 1, prev_m) | _ -> (0, max prev_m prev_c) )
    |> snd in
  let escaper = String.make (max_consecutive_backquotes + 1) '`' in
  String.concat ~sep:""
    [ escaper; (match s.[0] with '`' -> " " | _ -> "" | exception _ -> " "); s
    ; ( match s.[String.length s - 1] with
      | '`' -> " "
      | _ -> ""
      | exception _ -> " " ); escaper ]

let one_couple_few nb =
  match nb with
  | 0 -> "no"
  | 1 -> "one"
  | 2 | 3 | 4 -> "a couple of"
  | _ -> "a few"

(** [horizontal n] is a string representing a markdown horizontal line. n must
    be greater than 2. Merbocop uses this for parsing its own comments. Please
    don't confuse Merbocop by using the same number [n] in multiple places. See
    Delimiter module below. *)
let horizontal int =
  let rec aux i s = match i with 0 -> "" ^ s | i -> aux (i - 1) ("-" ^ s) in
  aux int ""

module Delimiters = struct let comment_footer = horizontal 6 end
