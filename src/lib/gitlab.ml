open! Base

let api_base_uri () = Uri.of_string "https://gitlab.com/api/v4"

let make_uri ?token ?(pagination = false) ?(parameters = []) path =
  let ( // ) = Caml.Filename.concat in
  let per_page = 100 in
  let get_pages =
    match pagination with
    | true ->
        [("pagination", ["keyset"]); ("per_page", [Int.to_string per_page])]
    | false -> [] in
  let actual_params =
    match token with
    | None -> parameters @ get_pages
    | Some t -> ("private_token", [t]) :: parameters @ get_pages in
  let base = api_base_uri () in
  let path = Uri.path base // path in
  Uri.with_path base path |> fun u -> Uri.add_query_params u actual_params

let opened_merge_requests ?(wip_status = `Any) ?token ~project ~retry_options ()
    =
  let wip_arg =
    (* See "wip" argument here:
       https://docs.gitlab.com/ee/api/merge_requests.html *)
    match wip_status with
    | `Only_ready -> [("wip", ["no"])]
    | `Only_wip -> [("wip", ["yes"])]
    | `Any -> [] in
  Web_api.get_json ?private_token:token ~pagination:true ~retry_options
    (make_uri ~pagination:true
       ~parameters:
         ( [ ("state", ["opened"]); ("pagination", ["keyset"])
           ; ("order_by", ["updated_at"]); ("sort", ["desc"])
           ; ("per_page", ["100"]) ]
         @ wip_arg )
       (Fmt.str "projects/%s/merge_requests" project) )

let single_merge_request ?token ~project ~mr ~retry_options () =
  Web_api.get_json ?private_token:token ~retry_options ~pagination:false
    (make_uri ~parameters:[]
       (Fmt.str "projects/%s/merge_requests/%d" project mr) )

let merge_request_commits ?token ~project ~mr ~retry_options () =
  Web_api.get_json ?private_token:token ~retry_options ~pagination:true
    (make_uri ~pagination:true
       (Fmt.str "projects/%s/merge_requests/%d/commits" project mr) )

(** [add_mr_label token project mr_id label] Gitlab API call adds a list of
    [labels] to a merge reqest. *)
let add_mr_labels ?(token : string option) ~(project : string) ~(mr_id : int)
    ~retry_options (labels : string list) =
  Web_api.get_json ~request:`PUT ?private_token:token ~retry_options
    ~pagination:false
    (make_uri ~parameters:[("add_labels", labels)]
       (Fmt.str "projects/%s/merge_requests/%d" project mr_id) )

(** [remove_mr_labels token project mr_id label] Gitlab API call removes a list
    of [labels] from a merge reqest. *)
let remove_mr_labels ?(token : string option) ~(project : string) ~(mr_id : int)
    ~retry_options (labels : string list) =
  Web_api.get_json ~request:`PUT ?private_token:token ~retry_options
    ~pagination:false
    (make_uri
       ~parameters:[("remove_labels", labels)]
       (Fmt.str "projects/%s/merge_requests/%d" project mr_id) )

(** [approve_merge_request token project mr_id] Gitlab API call to approve a
    merge reqest *)
let approve_merge_request ?(token : string option) ~(project : string)
    ~retry_options (mr_id : int) =
  Web_api.get_json ~request:`POST ?private_token:token ~retry_options
    ~pagination:false
    (make_uri (Fmt.str "projects/%s/merge_requests/%d/approve" project mr_id))

(** [unapprove_merge_request token project mr_id] Gitlab API call to unapprove a
    merge reqest. *)
let unapprove_merge_request ?(token : string option) ~(project : string)
    ~retry_options (mr_id : int) =
  Web_api.get_json ~request:`POST ?private_token:token ~retry_options
    ~pagination:false
    (make_uri
       (Fmt.str "projects/%s/merge_requests/%d/unapprove" project mr_id) )

(** [merge_request_approvals projet mr_id] returns EZJson.value resulting from a
    Gitlab API. *)
let merge_request_approvals ~(project : string) ~(mr_id : int) ~retry_options =
  Web_api.get_json ~retry_options ~pagination:false
    (make_uri ~pagination:false
       (Fmt.str "projects/%s/merge_requests/%d/approvals" project mr_id) )

(** [post_issue_thread token project retry_options issue_id body] returns the
    Ezjsonm.value resuling form a Gitlab API call which posts [body] to a
    discusion thread on [issue_id] in [project]. *)
let post_issue_thread ?(token : string option) ~(project : int)
    ~(retry_options : Web_api.Retry_options.t) ~(issue_id : int) body : Jq.t =
  Web_api.get_json ~request:`POST ?private_token:token ~retry_options
    ~pagination:false ~data_arg:("body", body)
    (make_uri (Fmt.str "projects/%d/issues/%d/discussions" project issue_id))
