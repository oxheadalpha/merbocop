# Merbocop: Merge-Request Bot Police 🤖

## Build

Get dependencies:

    opam switch create . 4.12.1  # Or provide a 4.12.1 OPAMSWITCH
    ./please.sh get_deps

and build:

    ./please.sh build

## Use

To run Merbocop use the `full-inspect` argument, point Merbocop at a Gitlab project and provide the number of merger request that you would like Merbocop to inspect.

For example:
``` bash
$ dune exec ./merbocop -- full-inspect --whole-project-inspection [PROJECT-ID:LIMIT]
```

`whole-project-inspection` provides Merbocop with the Gitlab API `PROJECT-ID` and a `LIMIT` for the number of merge request to inspect. 

The bot user will require "Developer" status in order to label and approve merge requests. (See [Gitlab Permissions](https://docs.gitlab.com/ee/user/permissions.html)). It may also be necessary to include the bot user as an eligible approver depending on the projects approval rules (see [Gitlab approval rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html#merge-request-approval-rules)).

## Options

``` bash
--self-id [USER-ID] 
```

The gitlab user ID number for the bot user. Merbocop uses the bot user's `USER-ID` to post/edit comments and approve merge request.

``` bash
--commits-to-inspect [PROJECT:COMMIT]
```

Provide Merbocop with a specific Gitlab project ID and commit ID to inspect.


``` bash
--only-updated-in-the-past [NUM:days]
```

Restrict Merbocop to inspect merger requests updated in the last `NUM:days`.

``` bash
--dump-cache [PATH]
```

Dump the contents of the cache to `PATH`.

``` bash
--load-cache [PATH]
```

Load the contents of the cache from `PATH`.

``` bash
--write-body [PATH]
```

Write the body of Merbocop's message output to `PATH`. This produces a markdown file.

``` bash
--access-token [TOKEN]
```

Provide the Gitlab access `TOKEN` required for some of Merbocop's options. Be sure the access token has the appropriate scope for the given actions you would like Merbocop to preform. See the [Gitlab docs regarding access token scope](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#limiting-scopes-of-a-project-access-token).  

``` bash
--post-on-self
```

Post or edit comments on merger request. This requires the `--access-token` option.

``` bash
--no-comment-editing
```

Always add merger request comments without editing existing ones.

``` bash
--set-doc-label [LABEL]
```

Add a merger request label (`LABEL`) to all merge requests which make changes to **.rst** or **.md** files only. This option will also remove the label if it doesn't apply. This requires the `--access-token` option.

``` bash
--set-approved-label [LABEL]
```

Add a merger request label (`LABEL`) to all merge requests which have enough approvals to be merged. This will also remove the label if it doesn't apply. 
This requires the `--access-token` option.

``` bash
--approve-doc-only
```

Merbocop will approve merge requests which make changes to **.rst** or **.md** files only. This will also revoke approval form a previously approved merge requests, if it's no longer doc-only. This requires the `--access-toke` option. 

``` bash
--allow-failures
```

Will return 0 exit status for some errors.

``` bash
--http-retry-options [RETRY-LIMIT:WAIT:FACTOR]
```

Will set the parameters used to retry failed HTTP calls. Failed calls will be retried (`RETRY-LIMIT`) times. The first retry will be delayed by (`WAIT`) seconds. Subsequent retries will be increasingly delayed by (`FACTOR`). Default values are: retry-limit = 4, wait =  0.1,  factor = 2.

``` bash
--http-no-retry
```

Will not retry failed HTTP calls. 

``` bash
--post-warnings-to [PROJECT]:[ISSUE]:[DEBUGGING]
```

Will post merge reqeusts warnings as Gitlab "discussions" under an (`ISSUE`) in (`PROJECT`). A separate discussion will be created for each merge request. (`DEBUGGING`) can be "always" to post empty warnings or, "non-empty" to skip merge requests that have no warnings. 

## Deployment

> The _old_ Gitlab-based deployment is kept here in case we need to revert
> easily:
> [oxheadalpha/merbocop/snippets/3610655](https://gitlab.com/oxheadalpha/merbocop/-/snippets/3610655).

See [src/edit-ci/main.ml](src/edit-ci/main.ml).

With `Merbohost` being a basic host with `systemd` and `docker`:

- A regular _“click-and-launch”_ Ubuntu cloud host should be enough: `sudo apt
  install docker.io` and `sudo adduser ubuntu docker`.
- For practical purposes the unix-user running the cron-jobs (`ubuntu` above)
  should be “allowed to linger” by `longind` (`sudo loginctl enable-linger
  <username>` and check with `loginctl user-status ubuntu`).

To make and upload a deployment archive (shell scripts and systemd user
configuration files):

```sh
dune exec src/edit-ci/main.exe -- systemd-configuration --format tar -o t0.tar
scp t0.tar Merbohost:
ssh Merbohost tar xvf t0.tar
```

The (generated) management script is then called `mpm`:

```default
 $ ssh Merbohost .local/bin/mpm help
Commands
* 'warm-up': Pull docker images, reload systemd-user.
* 'stop-all': Stop and disable all the pipelines.
* 'start-test': Enable and start the testing pipelines.
* 'start-all': Enable and start all the pipelines.
* 'report': Make a monitoring Markdown report (optional first argument: output file).
* 'wtf': Quickly output status of the pipelines.
Help:Done
```

The jobs use a configuration/secrets file at `~/.config/merbocop/sh.env`:

 ```sh
echo "Loading Merboconf" >&2

export merbocout=$HOME/merbocop-output/ # Where to put logs/reports
mkdir -p "$merbocout"

export smbot_token=XXXXXXXXXXXXX  # Authemntication token for the bot's test-user

export tezbocop_token=XXXXXXXXXX #  Authemntication token the production-user

export extra_docker_options="--cpus=1 --memory=1g" # Configure docker calls

export after_report=$HOME/.local/bin/some-script-to-run-on-the-reports.sh
```

If `$after_report` is not present, the scripts just display `wc -l <report>`,
the script can be used to for instance send the reports to an email address or
Slack channel.

The authentication tokens are regular Gitlab API tokens, the corresponding User-IDs are
**not configurable yet,** cf. [src/edit-ci/main.ml](src/edit-ci/main.ml):

```ocaml
let smbot_user = "4777401" in
let tzbot_user = "4984456" in
```
